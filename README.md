# Docker Swarm Local

## About

The purpose of "Docker Swarm Local" is to speed up development and deployment testing through reduction of overhead. "Docker Swarm Local" is intended to provide all of the services required in order to simulate deploying to Docker Swarm without any of the network dependencies (i.e. Bamboo, official Private Registry, and official Swarms).

### Prerequisites

**Install Jupyter**

The Demo is written as a Jupyter Notebook. First install Jupyter by following instructions at http://jupyter.org/install. Or, as preferred install via the Anaconda Distribution: https://www.anaconda.com/download/

**Install Bash Kernel**

The Demo is written as a Bash notebook, so we'll need to install the Bash Kernel next as defined in https://github.com/takluyver/bash_kernel

Run the following command to install:

```
pip install bash_kernel
python -m bash_kernel.install
```

### Running Jupyter

From the root of the project directory, run the following command:

```
jupyter notebook
```

Jupyter will launch in a Web-Browser and you should see jupyter-notebook .ipynb. Select it to open the notebook. Follow along by running each step in the notebook.

### Services of Docker Swarm Dev

+ [Registry](https://hub.docker.com/_/registry)
+ [Traefik](https://hub.docker.com/_/traefik)
+ [Docker-Registry-Frontend](https://hub.docker.com/r/konradkleine/docker-registry-frontend/)  
+ [Portainer](https://hub.docker.com/r/portainer/portainer/)

#### Humbly recreated from this tutorial video

<a href="http://www.youtube.com/watch?feature=player_embedded&v=Amp1RrrpGnk
" target="_blank"><img src="http://img.youtube.com/vi/Amp1RrrpGnk/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="560" height="315" border="10" /></a>
